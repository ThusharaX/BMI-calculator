# BMI-calculator

BMI-calculator is a Python progrme for calculate BMI

## Requirements

Python 3.8.2
[Download](https://www.python.org/downloads/release/python-382/)

## Installation

```bash
git clone https://github.com/ThusharaX/BMI-calculator.git
cd BMI-calculator
```
## Usage

```bash
python main.py
```

```
λ python main.py
Enter Name : Thushara
Height (m) : 1.8
Weight (kg) : 75

BMI : 23.148148148148145
Thushara is Normal weight

Press ENTER to check again
    Press "q" to quit :
q
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

## License
[MIT](https://choosealicense.com/licenses/mit/)